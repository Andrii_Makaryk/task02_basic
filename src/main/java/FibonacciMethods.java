public interface FibonacciMethods {

    void printSequance(int startSequance, int endSequance);

    void calculateSum(int startSequance, int endSequance);

    void printBiggestNumbers(int size);

    void percentageOfOddAndEven(int size);
}
