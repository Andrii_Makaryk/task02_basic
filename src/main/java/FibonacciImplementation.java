
class FibonacciImplementation implements FibonacciMethods {

    public void printSequance(int startSequance, int endSequance) {
        System.out.println("Odd sequance: ");
        for (int i = startSequance; i <= endSequance; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\nEven sequance:");
        for (int j = endSequance; j >= startSequance; j--) {
            if (j % 2 == 0) {
                System.out.print(j + " ");
            }
        }
    }

    public void calculateSum(int startSequance, int endSequance) {
        int sumAllEven = 0;
        int sumAllOdd = 0;
        for (int i = startSequance; i <= endSequance; i++) {
            if (i % 2 == 0) {
                sumAllEven += i;
            } else if (i % 2 != 0) {
                sumAllOdd += i;
            }
        }
        System.out.print("Sum all even numbers: " + sumAllEven + " and sum all odd numbers: " + sumAllOdd);
    }

    public void printBiggestNumbers(int size) {

        int firstNumberFibonacci = 1;
        int secondNumberFibonacci = 1;
        int sumOfFibonacci;
        int count = 0;
        int countTwo = 0;
        int arrayOddNumbers[];
        int arrayEvenNumbers[];
        int maxOddNumberFibonacci = 0;
        int maxEvenNumberFibonacci = 0;
        for (int i = 0; i < size; i++) {
            sumOfFibonacci = firstNumberFibonacci + secondNumberFibonacci;
            firstNumberFibonacci = secondNumberFibonacci;
            secondNumberFibonacci = sumOfFibonacci;
            if (sumOfFibonacci % 2 == 0) {
                count++;
                arrayOddNumbers = new int[count];
                for (int j = 0; j < arrayOddNumbers.length; j++) {
                    arrayOddNumbers[j] = sumOfFibonacci;
                    if (arrayOddNumbers[j] > maxOddNumberFibonacci)
                        maxOddNumberFibonacci = arrayOddNumbers[j];
                }
            } else if (sumOfFibonacci % 2 != 0) {
                countTwo++;
                arrayEvenNumbers = new int[countTwo];

                for (int j = 0; j < arrayEvenNumbers.length; j++) {
                    arrayEvenNumbers[j] = sumOfFibonacci;
                    if (arrayEvenNumbers[j] > maxEvenNumberFibonacci)
                        maxEvenNumberFibonacci = arrayEvenNumbers[j];
                }
            }
        }
        System.out.println("Maximal Fibonacci odd number: " + maxOddNumberFibonacci
                + " and maximal even Fibonacci namber: " + maxEvenNumberFibonacci);
    }

    public void percentageOfOddAndEven(int size) {
        int firstNumberFibonacci = 1;
        int secondNumberFibonacci = 1;
        int sumOfFibonacci;
        int countOdd = 0;
        double persentRepresentationOddNumbers = 0;
        double persentRepresentEven = 0;
        for (int i = 0; i < size; i++) {
            sumOfFibonacci = firstNumberFibonacci + secondNumberFibonacci;
            firstNumberFibonacci = secondNumberFibonacci;
            secondNumberFibonacci = sumOfFibonacci;
            if (sumOfFibonacci % 2 != 0)
                countOdd++;
        }
        persentRepresentationOddNumbers = (countOdd * 100) / size;
        persentRepresentEven = 100 - persentRepresentationOddNumbers;
        System.out.println(
                "Persentage of odd: " + persentRepresentationOddNumbers + "% and even: " + persentRepresentEven + "%");
    }
}
